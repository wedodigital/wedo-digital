import React, { Component, Fragment } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import classnames from 'classnames'
import Typed from 'react-typed'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import WorkList from '@organisms/WorkList'
import BlogList from '@organisms/BlogList'
import Loading from '@atoms/Loading'
import Heading from '@atoms/Heading'
import styles from './HomePage.scss'

class HomePage extends Component {
  state = {
    typed: this.props.location.state ? this.props.location.state.typed : false
  }

  stopTyping = () => {
    this.setState({typed: true})
  }

  render () {
    const typedArrays = [
      'development',
      'design',
      'branding',
      'ecommerce',
      'ux',
      'digital'
    ]

    const getWork = gql`{
      works( first: 4 ) {
        edges {
          node {
            title
            slug
            featuredImage {
            	sourceUrl(size: MEDIUM)
            }
            workMeta {
              launchDate
            }
          }
        }
      }
    }`

    const getBlog = gql`{
      posts( first: 4 ) {
        edges {
          node {
            title
            date
            slug
            featuredImage {
              sourceUrl(size: POST_THUMB)
            }
          }
        }
      }
    }`

    const headingClasses = classnames(
      styles['feature-heading'], {
        [styles['feature-heading--typed']]: this.state.typed
      }
    )

    return (
      <Fragment>
        <div className={styles['flood']}>
          <div>
            <div className={headingClasses}>
              <Heading
                level={1}
                weight='light'
                size='extra-large'
                headingStyle='homepage'
              >
                wedo.
                <span>
                  {this.state.typed ? 'digital' :
                    <Typed
                      strings={typedArrays}
                      typeSpeed={50}
                      showCursor={false}
                      onComplete={this.stopTyping}
                    />
                  }
                </span>
              </Heading>
            </div>
            {this.state.typed &&
              <FontAwesomeIcon
                className={styles['icon']}
                icon={['fal','arrow-circle-down']}
              />
            }
          </div>
        </div>
        {this.state.typed &&
          <Fragment>
            <Query query={getWork}>
              {
                ({ loading, error, data }) => {
                  if (loading) {
                    return <Loading />
                  }
                  return (
                    <Fragment>
                      <Heading headingStyle='white' level={1}>Latest Work</Heading>
                      <WorkList data={data.works.edges} />
                    </Fragment>
                  )
                }
              }
            </Query>
            <Query query={getBlog}>
              {
                ({ loading, error, data }) => {
                  if (loading) {
                    return <Loading />
                  }
                  return (
                    <Fragment>
                      <Heading headingStyle='white' level={1}>Latest Blog Posts</Heading>
                      <BlogList data={data.posts.edges} />
                    </Fragment>
                  )
                }
              }
            </Query>
          </Fragment>
        }
      </Fragment>
    )
  }
}

export default HomePage
