import React, { Component } from 'react'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'
import Page from '@molecules/Page'
import Heading from '@atoms/Heading'
import Loading from '@atoms/Loading'

class About extends Component {
  render () {
    const data = this.props.data.pageBy
    console.log(data)

    if(!data) {
      return <Loading />
    }

    return (
      <Page>
        <Heading level={1}>{data.title}</Heading>
        <div dangerouslySetInnerHTML={{__html: data.content}}></div>
      </Page>
    )
  }
}

const getPage = gql`query GET_PAGE_BY_URI($uri: String) {
    pageBy(uri: $uri) {
      id
      pageId
      title
      date
      uri
      content
    }
  }`

export default graphql(getPage, {
  options: (props) => {
    const uri = 'about'
    return {
      variables: {
        uri
      }
    }
  }
})(About)
