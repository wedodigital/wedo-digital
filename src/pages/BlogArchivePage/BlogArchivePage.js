import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import BlogList from '@organisms/BlogList'
import Loading from '@atoms/Loading'

class BlogArchivePage extends Component {
  render () {

    const getBlog = gql`{
      posts( first: 100 ) {
        edges {
          node {
            title
            date
            slug
            featuredImage {
              sourceUrl(size: POST_THUMB)
            }
          }
        }
      }
    }`

    return (
      <Query query={getBlog}>
        {
          ({ loading, error, data }) => {
            if (loading) {
              return <Loading />
            }
            return (
              <BlogList data={data.posts.edges} />
            )
          }
        }
      </Query>
    )
  }
}

export default BlogArchivePage
