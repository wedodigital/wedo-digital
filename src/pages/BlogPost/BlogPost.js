import React, { Component } from 'react'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'
import Page from '@molecules/Page'
import Loading from '@atoms/Loading'
import Heading from '@atoms/Heading'
import ContentBlock from '@molecules/ContentBlock'
import FeatureBackground from '@molecules/FeatureBackground'

class BlogPost extends Component {
  render () {
    const data = this.props.data.post

    if(!data) {
      return <Loading />
    }

    return (
      <Page width='full'>
        <FeatureBackground backgroundImage={this.props.data.post.featuredImage.sourceUrl}>
          <Heading level={1}>{data.title}</Heading>
        </FeatureBackground>
        <ContentBlock>
          <div dangerouslySetInnerHTML={{ __html: data.content }}></div>
        </ContentBlock>
      </Page>
    )
  }
}

const getPostBySlug = gql`query getPostBySlug($slug: String) {
  post: postBy(slug: $slug) {
    title
    content
    featuredImage {
      sourceUrl(size: POST_FEATURE)
    }
  }
}`

export default graphql(getPostBySlug, {
  options: (props) => {
    const slug = props.match.params.slug
    return {
      variables: {
        slug
      }
    }
  }
})(BlogPost)
