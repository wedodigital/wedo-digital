import React, { Component } from 'react'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'
import LazyLoad from 'react-lazyload'
import Page from '@molecules/Page'
import Loading from '@atoms/Loading'
import Heading from '@atoms/Heading'
import Screenshot from '@molecules/Screenshot'
import ContentBlock from '@molecules/ContentBlock'
import styles from './WorkPost.scss'

class WorkPost extends Component {
  render () {
    const data = this.props.data.work
    const gradColour = Math.ceil(Math.random() * 10)

    if(!data) {
      return <Loading />
    }

    return (
      <Page width='full'>
        <Screenshot
          height='medium'
          title={data.title}
          gradColour={this.props.location.state ? this.props.location.state.gradColour : gradColour}
          image={data.featuredImage.sourceUrl}
        />
        <ContentBlock>
          <div className={styles['work-meta']}>
            {data.workMeta.launchDate && <span>Launched {data.workMeta.launchDate}</span>}
            {data.workMeta.url && <span><a href={data.workMeta.url}>{data.workMeta.url}</a></span>}
          </div>
          <Heading level={1}>{data.title}</Heading>
          <div dangerouslySetInnerHTML={{ __html: data.content }}></div>
        </ContentBlock>
        {
          data.workMeta.screenshots.map((screenshot, i) => (
            <LazyLoad>
              <Screenshot
                image={screenshot.screenshot.sourceUrl}
                gradColour={i+1}
              />
            </LazyLoad>
          ))
        }
      </Page>
    )
  }
}

const getWorkBySlug = gql`query getWorkBySlug($slug: String) {
  work: workBy(uri: $slug) {
    title
    content
    featuredImage {
      sourceUrl(size: LARGE)
    }
    workMeta {
      launchDate
      url
      screenshots {
        screenshot {
          sourceUrl
        }
      }
    }
  }
}`

export default graphql(getWorkBySlug, {
  options: (props) => {
    const slug = props.match.params.slug
    return {
      variables: {
        slug
      }
    }
  }
})(WorkPost)
