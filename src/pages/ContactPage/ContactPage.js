import React, { Component } from 'react'
import Page from '@molecules/Page'
import Heading from '@atoms/Heading'
import ContactForm from '@organisms/ContactForm'

class ContactPage extends Component {

  render () {
    return (
      <Page>
        <Heading level={1}>Contact</Heading>
        <ContactForm />
      </Page>
    )
  }
}

export default ContactPage
