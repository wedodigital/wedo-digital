import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import WorkList from '@organisms/WorkList'
import Loading from '@atoms/Loading'

class WorkArchivePage extends Component {
  render () {

    const getWork = gql`{
      works( first: 100 ) {
        edges {
          node {
            title
            slug
            featuredImage {
            	sourceUrl(size: MEDIUM)
            }
            workMeta {
              launchDate
            }
          }
        }
      }
    }`

    return (
      <Query query={getWork}>
        {
          ({ loading, error, data }) => {
            if (loading) {
              return <Loading />
            }
            return (
              <WorkList data={data.works.edges} />
            )
          }
        }
      </Query>
    )
  }
}

export default WorkArchivePage
