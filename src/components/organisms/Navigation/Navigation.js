import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Button from '@atoms/Button'
import Modal from '@molecules/Modal'
import styles from './Navigation.scss'

class Navigation extends Component {

  state = {
    open: false
  }

  toggleModal = () => {
    this.setState({
      open: !this.state.open
    })
  }

  render () {
    return (
      <Fragment>
        <Modal open={this.state.open}>
          <nav className={styles['navigation']}>
            <ul>
              <li>
                <NavLink
                  onClick={this.toggleModal}
                  activeClassName={styles['active']}
                  to={{
                    pathname: '/',
                    state: {
                      typed: true
                    }
                  }}
                >
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink
                  onClick={this.toggleModal}
                  activeClassName={styles['active']}
                  to="/works"
                >
                  Work
                </NavLink>
              </li>
              <li>
                <NavLink
                  onClick={this.toggleModal}
                  activeClassName={styles['active']}
                  to="/blog"
                >
                  Blog
                </NavLink>
              </li>
            </ul>
          </nav>
        </Modal>
        <span className={styles['trigger']}>
          <Button
            onClick={this.toggleModal}
            appearance={this.state.open ? 'secondary' : 'primary'}
            buttonStyle='icon-button'
          >
            <FontAwesomeIcon
              icon={this.state.open ? ['fal','times'] : ['fal','bars']}
            />
          </Button>
        </span>
      </Fragment>
    )
  }
}

export default Navigation
