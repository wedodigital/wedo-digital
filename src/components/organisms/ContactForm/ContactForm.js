import React, { Component } from 'react'
import Button from '@atoms/Button'
import axios from 'axios'

const API_PATH = 'http://localhost/wedo-api/contact/index.php'

class ContactForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      message: '',
      mailSent: false,
      error: null
    }
  }

  handleFormSubmit = e => {
    e.preventDefault();
    axios({
      method: 'post',
      url: `${API_PATH}`,
      headers: { 'content-type': 'application/json' },
      data: this.state
    })
    .then(result => {
      this.setState({
        mailSent: result.data.sent
      })
    })
    .catch(error => this.setState({ error: error.message }))
  }

  render () {
    return (
      <form action="#">
        <label>First Name</label>
        <input
          type="text"
          id="name"
          name="firstname"
          placeholder="Your name"
          value={this.state.name}
          onChange={e => this.setState({ name: e.target.value })}
        />

        <label>Email</label>
        <input
          type="email"
          id="email"
          name="email"
          placeholder="Your email"
          value={this.state.email}
          onChange={e => this.setState({ email: e.target.value })}
        />

        <label>Message</label>
        <textarea
          id="message"
          name="message"
          placeholder="Write something.."
          onChange={e => this.setState({ message: e.target.value })}
          value={this.state.message}
        >
        </textarea>
        <Button onClick={e => this.handleFormSubmit(e)} appearance='primary'>Submit</Button>

        {this.state.mailSent &&
          <div>Thank you for contcting us.</div>
        }
      </form>
    )
  }

}

export default ContactForm
