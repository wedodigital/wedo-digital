import React, { Component } from 'react'
import BlogCard from '@molecules/BlogCard'
import styles from './BlogList.scss'

class BlogList extends Component {
  render () {
    return (
      <div className={styles['blog-list']}>
        {
          this.props.data.map((blog, key) => {
            return (
              <BlogCard key={key} data={blog.node} />
            )
          })
        }
      </div>
    )
  }
}

export default BlogList
