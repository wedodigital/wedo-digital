import React, { Component } from 'react'
import WorkCard from '@molecules/WorkCard'
import styles from './WorkList.scss'

class WorkList extends Component {
  render () {
    return (
      <div className={styles['work-list']}>
        {
          this.props.data.map((work, key) => {
            return (
              <WorkCard
                key={key}
                data={work.node}
              />
            )
          })
        }
      </div>
    )
  }
}

export default WorkList
