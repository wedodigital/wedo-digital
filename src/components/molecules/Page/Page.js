import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import styles from './Page.scss'

class Page extends Component {

  static propTypes = {
    width: PropTypes.oneOf('full', 'narrow'),
    children: PropTypes.node
  }

	render() {

    const pageClasses = [
      styles['page'],
      styles[`${this.props.width}`]
    ].join(' ')

    const contentClasses = [
      styles['page-content'],
      styles[`${this.props.width}`]
    ].join(' ')

		return (
      <Fragment>
        <section className={pageClasses}>
          <section className={contentClasses}>
    				{this.props.children}
          </section>
          <div className={styles['footer-w']}></div>
        </section>
      </Fragment>
		)
	}
}

export default Page
