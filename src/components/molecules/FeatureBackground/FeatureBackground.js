import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import styles from './FeatureBackground.scss'

class FeatureBackground extends Component {

  static propTypes = {
    gradColour: PropTypes.oneOf(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']),
    backgroundImage: PropTypes.string,
    children: PropTypes.node
  }

	render() {
    const backgroundClasses = classnames(
      styles['feature-background'],
    )

    const titleClasses = classnames(
      styles['feature-background__title'],
      {
        [styles['feature-background__title--underlay']]: this.props.backgroundImage
      }
    )

		return (
      <section className={backgroundClasses}>
        <section
          style={this.props.backgroundImage && {
            backgroundImage: `url(${this.props.backgroundImage})`}
          }
          className={styles['feature-background__image']}
        >
  				{this.props.children &&
            <div className={titleClasses}>
              {this.props.children}
            </div>
          }
        </section>
      </section>
		)
	}
}

export default FeatureBackground
