import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from './Modal.scss'
import classnames from 'classnames'

class Modal extends Component {

  static propTypes = {
    open: PropTypes.bool,
    children: PropTypes.node
  }

  render () {
    const modalClasses = classnames(
      styles['modal'],
      {
        [styles['display-block']]: this.props.open === true,
        [styles['display-none']]: this.props.open === false,
      }
    )

    return (
      <div className={modalClasses}>
        <section className={styles['modal-main']}>
          {this.props.children}
        </section>
      </div>
    )
  }
}

export default Modal
