import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Heading from '@atoms/Heading'
import styles from './BlogCard.scss'

class BlogCard extends Component {

  static propTypes = {
    data: PropTypes.shape({
      date: PropTypes.string,
      slug: PropTypes.string,
      title: PropTypes.string,
      featuredImage: PropTypes.shape({
        sourceUrl: PropTypes.string
      })
    })
  }

  get publishDate () {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    const dateTime = this.props.data.date.split('T')
    const fullDate = dateTime[0].split('-')

    const day = fullDate[2].replace(/^0+/, '')
    const month = months[fullDate[1].replace(/^0+/, '') - 1]
    const year = fullDate[0].replace(/^0+/, '')

    return `${day}th ${month} ${year}`
  }

  render () {
    return (
      <Link to={`/blog/${this.props.data.slug}`} className={styles['blog-card']}>
        <div className={styles['blog-card__date']}>
          {this.publishDate}
        </div>
        <div className={styles['blog-card__image']}>
          <img src={this.props.data.featuredImage.sourceUrl} alt={this.props.data.title} />
        </div>
        <div className={styles['blog-card__title']}>
          <Heading level={5}>{this.props.data.title}</Heading>
          <FontAwesomeIcon className={styles['loading']} icon={['fal','angle-right']} />
        </div>
      </Link>
    )
  }
}

export default BlogCard
