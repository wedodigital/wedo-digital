import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Heading from '@atoms/Heading'
import Screenshot from '@molecules/Screenshot'
import styles from './WorkCard.scss'

class WorkCard extends Component {
  static propTypes = {
    data: PropTypes.shape({
      slug: PropTypes.string,
      title: PropTypes.string,
      featuredImage: PropTypes.string
    })
  }

  render () {

    let gradColour = Math.ceil(Math.random() * 10)

    return (
      <Link
        to={{
          pathname: `/works/${this.props.data.slug}`,
          state: { gradColour: gradColour }
        }}
        className={styles['work-card']}
      >
        <Screenshot
          height='small'
          gradColour={gradColour}
          title={this.props.data.title}
          image={this.props.data.featuredImage.sourceUrl}
        />
        <div className={styles['work-card__content']}>
          <div>
            <Heading level={6}>{this.props.data.title}</Heading>
            <span styleName={styles['launch-date']}>{this.props.data.workMeta.launchDate}</span>
          </div>
          <FontAwesomeIcon className={styles['loading']} icon={['fal','angle-right']} />
        </div>
      </Link>
    )
  }
}

export default WorkCard
