import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from './ContentBlock.scss'

class ContentBlock extends Component {
  static propTypes = {
      children: PropTypes.node
  }

	render() {
		return (
      <section className={styles['content-block']}>
				{this.props.children}
      </section>
		)
	}
}

export default ContentBlock
