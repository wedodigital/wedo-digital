import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import styles from './Screenshot.scss'

class Screenshot extends Component {

  static propTypes = {
    gradColour: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    height: PropTypes.oneOf('small', 'medium', 'large'),
    title: PropTypes.string,
    backgroundImage: PropTypes.string,
    children: PropTypes.node
  }

	render() {
    const backgroundClasses = classnames(
      styles['screenshot'],
      styles[`gradient-${this.props.gradColour}`],
      styles[`screenshot--${this.props.height}-height`]
    )

		return (
      <section className={backgroundClasses}>
        <div className={styles['screenshot-wrapper']}>
          <div className={styles['browser-bar']}>
            <span className={styles['browser-bar__button']} />
            <span className={styles['browser-bar__button']} />
            <span className={styles['browser-bar__button']} />
          </div>
          <img src={this.props.image} alt={this.props.title} />
        </div>
      </section>
		)
	}
}

export default Screenshot
