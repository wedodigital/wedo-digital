import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from './Button.scss'

class Button extends Component {

  static propTypes = {
    active: PropTypes.bool,
    appearance: PropTypes.oneOf(['primary', 'secondary']),
    buttonStyle: PropTypes.oneOf(['button', 'icon-button']),
    children: PropTypes.node
  }

  static defaultProps = {
    appearance: 'primary',
    buttonStyle: 'button'
  }

  handleClick (event) {
    event.preventDefault()
    this.props.onClick.apply(this, [event])
  }

  render () {

    const buttonClasses = [
      styles['button'],
      styles[`${this.props.appearance}`],
      styles[`${this.props.buttonStyle}`],
      this.props.active && styles['active']
    ].join(' ')

    return (
      <button
        className={buttonClasses}
        onClick={(event) => this.handleClick(event)}
      >
        {this.props.children}
      </button>
    )
  }
}

export default Button
