import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styles from './Logo.scss'

class Logo extends Component {
  render () {
    return (
      <Link className={styles['logo']} to='/'>W</Link>
    )
  }
}

export default Logo
