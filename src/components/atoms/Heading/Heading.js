import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from './Heading.scss'

class Heading extends Component {

  static propTypes = {
    level: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
    headingStyle: PropTypes.oneOf('default', 'homepage'),
    children: PropTypes.node
  }

  static defaultProps = {
    headingStyle: 'default'
  }

  render () {
    const headingClasses = [
      styles['heading'],
      styles[`heading-${this.props.level}`],
      styles[`heading-${this.props.headingStyle}`]
    ].join(' ')

    if (this.props.level > 6) return null

    const Component = `h${this.props.level}`

    return (
      <Component className={headingClasses}>
        { this.props.children }
      </Component>
    )
  }
}

export default Heading
