import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './Loading.scss'

class Loading extends Component {
  render () {
    return (
      <div className={styles['loading-wrapper']}>
        <FontAwesomeIcon
          className={styles['loading']}
          icon={['fal','spinner-third']}
          spin
          size='2x'
        />
      </div>
    )
  }
}

export default Loading
