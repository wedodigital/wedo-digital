import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SiteHeader from './components/SiteHeader'
import GradBackground from '@utilities/GradBackground'
import styles from './Layout.scss'

class Layout extends Component {

  static propTypes = {
    children: PropTypes.node
  }

	render() {
		return (
      <section className={styles['page-layout']}>
        <section className={styles['page-wrapper']}>
				  {this.props.children}
        </section>
        <SiteHeader />
				<GradBackground />
      </section>
		)
	}
}

export default Layout
