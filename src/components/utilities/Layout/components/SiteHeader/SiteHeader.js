import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Navigation from '@organisms/Navigation'
import Logo from '@atoms/Logo'
import styles from './SiteHeader.scss'

class SiteHeader extends Component {

  static propTypes = {
    children: PropTypes.node
  }

	render() {
		return (
      <section className={styles['site-header']}>
        <Logo />
        <Navigation />
      </section>
		)
	}
}

export default SiteHeader
