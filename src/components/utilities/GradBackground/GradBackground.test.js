import React from 'react'
import GradBackground from './GradBackground'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'

configure({
  adapter: new Adapter()
})

test('GradBackground will render when passed no props', () => {
  const component = shallow(<GradBackground />)
  expect(component.length).toEqual(1)
})
