import React, {Component} from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Layout from '@utilities/Layout'
import HomePage from '@pages/HomePage'
import About from '@pages/About'
import WorkArchivePage from '@pages/WorkArchivePage'
import WorkPost from '@pages/WorkPost'
import BlogArchivePage from '@pages/BlogArchivePage'
import BlogPost from '@pages/BlogPost'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'

import './App.scss'

const client = new ApolloClient({
  uri: 'https://cms.wedo.digital/graphql'
})

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <BrowserRouter>
          <Layout>
            <Switch>
              <Route exact path='/' component={HomePage} />
              <Route path='/about' component={About} />
              <Route path='/works/:slug' component={WorkPost} />
              <Route path='/works' component={WorkArchivePage} />
              <Route path='/blog/:slug' component={BlogPost} />
              <Route path='/blog' component={BlogArchivePage} />
            </Switch>
          </Layout>
        </BrowserRouter>
      </ApolloProvider>
    )
  }
}

export default App;
